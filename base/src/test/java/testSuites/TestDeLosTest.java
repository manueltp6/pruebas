package testSuites;


import org.testng.annotations.Test;
import testClasses.CPA0000Test;
import testClasses.Test_CPA;
import testClasses.CPA0001Test;


public class TestDeLosTest {
    @Test
    public void CPA00001() {
        CPA0000Test cpa0000 = new CPA0000Test();
        cpa0000.testPrueba();
    }

    @Test
    public void CPA00002() {
        CPA0001Test cpa0001 = new CPA0001Test();
        cpa0001.testPrueba();
    }

    @Test
    public void CPA00003() {
        Test_CPA testCpa = new Test_CPA();
        testCpa.testPrueba();
    }

}
